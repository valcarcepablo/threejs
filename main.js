import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader'
import { GUI } from 'three/addons/libs/lil-gui.module.min.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'

const newPosition = new THREE.Vector3();
const model = null;
const mixers = [];
const clock = new THREE.Clock();
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, innerWidth / innerHeight, 1, 2000 );
const renderer = new THREE.WebGLRenderer();
renderer.setSize( innerWidth, innerHeight );
document.body.appendChild( renderer.domElement );

const orbitControls = new OrbitControls(camera, renderer.domElement);
orbitControls.enableDamping = true
orbitControls.minDistance = 5
orbitControls.maxDistance = 15
orbitControls.enablePan = false
orbitControls.maxPolarAngle = Math.PI / 2 - 0.05
orbitControls.update();

const mesh = new THREE.Mesh( new THREE.PlaneGeometry( 100, 100 ), new THREE.MeshPhongMaterial( { color: 0x999999, depthWrite: false } ) );
mesh.rotation.x = - Math.PI / 2;
mesh.receiveShadow = true;
scene.add( mesh );
scene.background = new THREE.Color( 0xa0a0a0 );
scene.fog = new THREE.Fog( 0xe0e0e0, 20, 100 );
camera.position.set(10, 3, 2);
camera.lookAt(-3, -3, 0);

const hemiLight = new THREE.HemisphereLight( 0xffffff, 0x444444 );
				hemiLight.position.set( 0, 20, 0 );
				scene.add( hemiLight );


const dirLight = new THREE.DirectionalLight( 0xffffff );
dirLight.position.set( - 3, 10, - 10 );
dirLight.castShadow = true;
dirLight.shadow.camera.top = 2;
dirLight.shadow.camera.bottom = - 2;
dirLight.shadow.camera.left = - 2;
dirLight.shadow.camera.right = 2;
dirLight.shadow.camera.near = 0.1;
dirLight.shadow.camera.far = 40;
scene.add( dirLight );
const loader = new GLTFLoader();
loader.load( './assets/animations/KayKit_AnimatedCharacter_v1.2.glb', function ( gltf ) {

  this.model= gltf.scene;
  const model = gltf.scene;
  
  model.traverse( function ( object ) {

    if ( object.isMesh ) object.castShadow = true;

  } );
  model.rotation.y=3;
  model.position.z = -4;
  const mixer = new THREE.AnimationMixer( model );
  mixer.clipAction( gltf.animations[ 4 ] ).play();
  scene.add( model );
  mixers.push(mixer);
  document.addEventListener("keydown", onDocumentKeyDown, false);
  function onDocumentKeyDown(event) {
    var keyCode = event.which;
    if (keyCode == 87) {
        model.position.z -= 0.1;
    } else if (keyCode == 83) {
        model.position.z += 0.1;
    } else if (keyCode == 65) {
        model.position.x -= 0.1;
    } else if (keyCode == 68) {
        model.position.x += 0.1;
    }
};

  const numAnimations = gltf.animations.length;
  const animations = gltf.animations;
  animate(); 

} );

loader.load('./assets/scenario/scenario.glb', function (gltf ) {

  const model = gltf.scene;
  model.traverse( function ( object ) {

    if ( object.isMesh ) object.castShadow = true;

  } );
  scene.add( model );


})


function animate() {

  requestAnimationFrame( animate );

  const delta = clock.getDelta();

  for ( const mixer of mixers ) mixer.update( delta );
  renderer.render( scene, camera );
  camera.position = (this.model.position);
  camera.lookAt(this.model.position);
}

